#!/usr/bin/python3
# -*- coding: utf-8 -*-

# %%
from posixpath import split
import sys
import string
import random
from typing import TypedDict
import csv
import subprocess
import platform


class Individual(TypedDict):
    genome: list[list[float]]
    fitness: float


Population = list[Individual]

# Simulation Constants (do not change)
NUM_JOINTS = 3  # number of joints on the robot arm
TIME_RESOLUTION = 2  # frames the simulation waits between new input
NUM_INPUTS = 20  # number of input values given to each joint
AIR_TIME = 1.5  # seconds between disc release and distance check
MAX_VAL = 4.0  # Max value of any individual input value

# You can chenge this one
visualize = (
    False  # whether to run the graphics of the sim (runs slower, may crash on VMs)
)


def initialize_individual(genome: list[list[float]], fitness: float = 0) -> Individual:
    """
    Purpose:        Create one individual
    Parameters:     genome as string, fitness as integer (higher better)
    User Input:     no
    Prints:         no
    Returns:        One Individual, as a dict[str, int]
    Modifies:       Nothing
    Calls:          Basic python only
    Tests:          ./unit_tests/*
    Status:         Do this one!
    Example doctest:
    >>> initialize_individual("EC is fun", 10)
    {'genome': 'EC is fun', 'fitness': 10}
    >>> initialize_individual("Fun is EC", 9)
    {'genome': 'Fun is EC', 'fitness': 9}
    """
    return Individual(genome=genome, fitness=fitness)


def initialize_pop(pop_size: int) -> Population:
    """
    Purpose:        Create population to evolve
    Parameters:     Goal string, population size as int
    User Input:     no
    Prints:         no
    Returns:        a population, as a list of Individuals
    Modifies:       Nothing
    Calls:          random.choice, string.ascii_letters, initialize_individual
    Tests:          ./unit_tests/*
    Status:         Do this one!
    Example doctest:
    >>> import random
    >>> random.seed(42)
    >>> for _ in range(4):
    ...     initialize_pop("EC is easy", 2)
    [{'genome': 'OhbVrpoiVg', 'fitness': 0}, {'genome': 'RVIfLBcbfn', 'fitness': 0}]
    [{'genome': 'oGMbJmTPSI', 'fitness': 0}, {'genome': 'AoCLrZaWZk', 'fitness': 0}]
    [{'genome': 'SBvrjnWvgf', 'fitness': 0}, {'genome': 'ygwwMqZcUD', 'fitness': 0}]
    [{'genome': 'IhyfJsONxK', 'fitness': 0}, {'genome': 'mTecQoXsfo', 'fitness': 0}]
    """
    return [
        initialize_individual(
            [
                [random.uniform(-MAX_VAL, MAX_VAL) for _ in range(NUM_INPUTS)]
                for __ in range(NUM_JOINTS)
            ]
        )
        for _ in range(pop_size)
    ]


def recombine_pair(parent1: Individual, parent2: Individual) -> Population:
    """
    Purpose:        Recombine two parents to produce two children
    Parameters:     Two parents as Individuals
    User Input:     no
    Prints:         no
    Returns:        One Individual, as a TypedDict[str, int]
    Modifies:       Nothing
    Calls:          Basic python, random.choice, initialize_individual
    Tests:          ./unit_tests/*
    Status:         Do this one!
    Example doctest:
    >>> import random
    >>> random.seed(42)
    >>> i1 = initialize_individual("EC is great", 10)
    >>> i2 = initialize_individual("Great is EC", 10)
    >>> for _ in range(4):
    ...     recombine_pair(i1, i2)
    [{'genome': 'EC is greaC', 'fitness': 0}, {'genome': 'Great is Et', 'fitness': 0}]
    [{'genome': 'Ereat is EC', 'fitness': 0}, {'genome': 'GC is great', 'fitness': 0}]
    [{'genome': 'Great is EC', 'fitness': 0}, {'genome': 'EC is great', 'fitness': 0}]
    [{'genome': 'EC it is EC', 'fitness': 0}, {'genome': 'Greas great', 'fitness': 0}]
    """
    index_list = [random.randint(0, NUM_INPUTS - 1) for _ in range(NUM_JOINTS)]
    pop = []
    pop.append(
        initialize_individual(
            [
                parent1["genome"][i][0 : index_list[i]]
                + parent2["genome"][i][index_list[i] :]
                for i in range(NUM_JOINTS)
            ]
        )
    )
    pop.append(
        initialize_individual(
            [
                parent2["genome"][i][0 : index_list[i]]
                + parent1["genome"][i][index_list[i] :]
                for i in range(NUM_JOINTS)
            ]
        )
    )
    return pop


def recombine_group(parents: Population, recombine_rate: float) -> Population:
    """
    Purpose:        Recombines a whole group, returns the new population
    Parameters:     genome as string, fitness as integer (higher better)
    User Input:     no
    Prints:         no
    Returns:        New population of children
    Modifies:       Nothing
    Calls:          Basic python, recombine pair
    Tests:          ./unit_tests/*
    Status:         Do this one!
    Example doctest:
    >>> import random
    >>> random.seed(42)
    >>> i1 = initialize_individual("EC is great", 10)
    >>> i2 = initialize_individual("Great is EC", 10)
    >>> pop = [i1, i2]
    >>> for _ in range(4):
    ...     recombine_group(pop, 0.8)
    [{'genome': 'Great is EC', 'fitness': 0}, {'genome': 'EC is great', 'fitness': 0}]
    [{'genome': 'EC at is EC', 'fitness': 0}, {'genome': 'Greis great', 'fitness': 0}]
    [{'genome': 'Ereat is EC', 'fitness': 0}, {'genome': 'GC is great', 'fitness': 0}]
    [{'genome': 'EC is gr EC', 'fitness': 0}, {'genome': 'Great iseat', 'fitness': 0}]
    """
    new_pop = []
    for i in range(0, len(parents) - 1):
        if random.random() < recombine_rate:
            new_pop += recombine_pair(parents[i], parents[i + 1])
        # else:
        #     new_pop += [parents[i], parents[i + 1]]
    return new_pop


def mutate_individual(
    parent: Individual, mutate_rate: float, mutate_dist: float
) -> Individual:
    """
    Purpose:        Mutate one individual
    Parameters:     One parents as Individual, mutation rate as float (0-1)
    User Input:     no
    Prints:         no
    Returns:        One Individual, as a TypedDict[str, int]
    Modifies:       Nothing
    Calls:          Basic python, initialize_individual
    Tests:          ./unit_tests/*
    Status:         Do this one!
    Example doctest:
    >>> import random
    >>> random.seed(42)
    >>> ind = initialize_individual("EC is fun", 10)
    >>> for _ in range(4):
    ...     mutate_individual(ind, 0.3)
    {'genome': 'Eriis BfG', 'fitness': 0}
    {'genome': 'EC is auB', 'fitness': 0}
    {'genome': 'EW gs cun', 'fitness': 0}
    {'genome': 'EC is fuo', 'fitness': 0}
    """
    new_genome = parent["genome"]
    for joint in new_genome:
        for entry in joint:
            if random.random() < mutate_rate:
                # mutate
                entry = entry + random.gauss(mu=0, sigma=mutate_dist)
                # clamp to max value
                entry = max(-MAX_VAL, min(MAX_VAL, entry))

    return initialize_individual(new_genome)


def mutate_group(
    children: Population, mutate_rate: float, mutate_dist: float = 0.4
) -> Population:
    """
    Purpose:        Mutates a whole Population, returns the mutated group
    Parameters:     Population, mutation rate as float (0-1)
    User Input:     no
    Prints:         no
    Returns:        One Individual, as a TypedDict[str, int]
    Modifies:       Nothing
    Calls:          Basic python, mutate_individual
    Tests:          ./unit_tests/*
    Status:         Do this one!
    Example doctest:
    >>> import random
    >>> random.seed(42)
    >>> i1 = initialize_individual("EC is great", 10)
    >>> i2 = initialize_individual("Great is EC", 10)
    >>> pop = [i1, i2]
    >>> for _ in range(4):
    ...     mutate_group(pop, 0.3)
    [{'genome': 'Eriis BfGat', 'fitness': 0}, {'genome': 'Greaa BsWEg', 'fitness': 0}]
    [{'genome': 'ECcis great', 'fitness': 0}, {'genome': 'Groat is Ew', 'fitness': 0}]
    [{'genome': 'rC OUDgreat', 'fitness': 0}, {'genome': 'GrcatKis EC', 'fitness': 0}]
    [{'genome': 'Ep is greFd', 'fitness': 0}, {'genome': 'Gkeay iJ UI', 'fitness': 0}]
    """
    return [mutate_individual(child, mutate_rate, mutate_dist) for child in children]


def evaluate_group(individuals: Population) -> None:
    """
    Purpose:        Computes and modifies the fitness for population
    Parameters:     Objective string, Population
    User Input:     no
    Prints:         no
    Returns:        None
    Modifies:       The Individuals, all mutable objects
    Calls:          Basic python, evaluate_individual
    Tests:          ./unit_tests/*
    Status:         Do this one!
    Example doctest:
    >>> import random
    >>> random.seed(42)
    >>> i1 = initialize_individual("This assignment is work!", 0)
    >>> i2 = initialize_individual("This assignment is hard!", 0)
    >>> objective = "This assignment is easy!"
    >>> pop = [i1, i2]
    >>> evaluate_group(objective=objective, individuals=pop)
    >>> pop[0]
    {'genome': 'This assignment is work!', 'fitness': 20}
    >>> pop[1]
    {'genome': 'This assignment is hard!', 'fitness': 21}
    """

    # creating input file
    with open("input.txt", "w") as file:

        # header
        file.write(
            "{pop_size}\n{num_inputs}\n{time_res}\n{air_time}\n".format(
                pop_size=len(individuals),  # Population size
                num_inputs=NUM_INPUTS,  # Time entries
                time_res=TIME_RESOLUTION,  # Delay frames
                air_time=AIR_TIME,  # Air time
            )
        )

        csvwrite = csv.writer(file)

        for i in individuals:
            # Check the genome length
            if len(i["genome"]) != NUM_JOINTS:
                raise RuntimeError("Invalid genome! Incorrect number of input lists.")
            for inputs in i["genome"]:
                if len(inputs) != NUM_INPUTS:
                    raise RuntimeError(
                        "Invalid genome! Incorrect number of entries in input list."
                    )

            # Write this genome to input file
            csvwrite.writerows(i["genome"])

    # Evaluate
    match platform.system():
        case "Windows":
            if visualize == True:
                simulation = subprocess.run(
                    "./Godot_v4.0-beta3_win64.exe --main-pack RobotArmSimulator.pck --input-path=input.txt --output-path=output.txt".split(
                        " "
                    )
                )
            else:
                simulation = subprocess.run(
                    "./Godot_v4.0-beta3_win64.exe --main-pack RobotArmSimulator.pck --display-driver headless --input-path=input.txt --output-path=output.txt".split(
                        " "
                    )
                )
        case "Linux":
            if visualize == True:
                simulation = subprocess.run(
                    "./Godot_v4.0-beta3_linux.x86_64 --main-pack RobotArmSimulator.pck --input-path=input.txt --output-path=output.txt".split(
                        " "
                    )
                )
            else:
                simulation = subprocess.run(
                    "./Godot_v4.0-beta3_linux.x86_64 --main-pack RobotArmSimulator.pck --display-driver headless --input-path=input.txt --output-path=output.txt".split(
                        " "
                    )
                )

    if simulation.returncode != 0:
        raise RuntimeError("Simulation failed!")

    # Update fitnesses
    with open("output.txt", "r") as file:
        for i in individuals:
            fitness = float(file.readline())
            i["fitness"] = fitness

    # print(individuals)


def write_best(individual: Individual) -> None:
    # creating input file
    with open("final-answer.txt", "w") as file:
        # header
        file.write(
            "{pop_size}\n{num_inputs}\n{time_res}\n{air_time}\n".format(
                pop_size=1,  # Population size
                num_inputs=NUM_INPUTS,  # Time entries
                time_res=TIME_RESOLUTION,  # Delay frames
                air_time=AIR_TIME,  # Air time
            )
        )

        csvwrite = csv.writer(file)

        if len(individual["genome"]) != NUM_JOINTS:
            raise RuntimeError("Invalid genome! Incorrect number of input lists.")
        for inputs in individual["genome"]:
            if len(inputs) != NUM_INPUTS:
                raise RuntimeError(
                    "Invalid genome! Incorrect number of entries in input list."
                )

        # Write this genome to input file
        csvwrite.writerows(individual["genome"])


def rank_group(individuals: Population) -> None:
    """
    Purpose:        Create one individual
    Parameters:     Population of Individuals
    User Input:     no
    Prints:         no
    Returns:        None
    Modifies:       The population's order (a mutable object)
    Calls:          Basic python only
    Tests:          ./unit_tests/*
    Status:         Do this one!
    Example doctest:
    >>> import random
    >>> random.seed(42)
    >>> i1 = initialize_individual("Zhis glass it isabeesting!", 2)
    >>> i2 = initialize_individual("This class is motivating!!", 6)
    >>> objective = "This class is captivating!"
    >>> pop = [i1, i2]
    >>> rank_group(pop)
    >>> pop
    [{'genome': 'This class is motivating!!', 'fitness': 6}, {'genome': 'Zhis glass it isabeesting!', 'fitness': 2}]
    """
    individuals.sort(key=lambda x: x["fitness"], reverse=True)


def parent_select(individuals: Population, number: int) -> Population:
    """
    Purpose:        Choose parents in direct probability to their fitness
    Parameters:     Population, the number of individuals to pick.
    User Input:     no
    Prints:         no
    Returns:        Sub-population
    Modifies:       Nothing
    Calls:          Basic python, random.choices (hint)
    Tests:          ./unit_tests/*
    Status:         Do this one!
    Example doctest:
    >>> import random
    >>> random.seed(42)
    >>> i1 = initialize_individual("gene", 5)
    >>> i2 = initialize_individual("meme", 6)
    >>> i3 = initialize_individual("heme", 4)
    >>> pop = [i1, i2, i3]
    >>> for _ in range(4):
    ...     parent_select(pop, 2)
    [{'genome': 'meme', 'fitness': 6}, {'genome': 'gene', 'fitness': 5}]
    [{'genome': 'gene', 'fitness': 5}, {'genome': 'gene', 'fitness': 5}]
    [{'genome': 'heme', 'fitness': 4}, {'genome': 'meme', 'fitness': 6}]
    [{'genome': 'heme', 'fitness': 4}, {'genome': 'gene', 'fitness': 5}]
    """
    return random.choices(
        individuals, weights=[x["fitness"] + 6 for x in individuals], k=number
    )


def survivor_select(individuals: Population, pop_size: int) -> Population:
    """
    Purpose:        Picks who gets to live!
    Parameters:     Population, and population size to return.
    User Input:     no
    Prints:         no
    Returns:        Population, of pop_size
    Modifies:       Nothing
    Calls:          Basic python only
    Tests:          ./unit_tests/*
    Status:         Do this one!
    Example doctest:
    >>> import random
    >>> random.seed(42)
    >>> i1 = initialize_individual("meme", 6)
    >>> i2 = initialize_individual("gene", 5)
    >>> i3 = initialize_individual("heme", 4)
    >>> pop = [i1, i2, i3]
    >>> for _ in range(4):
    ...     survivor_select(pop, 2)
    [{'genome': 'meme', 'fitness': 6}, {'genome': 'gene', 'fitness': 5}]
    [{'genome': 'meme', 'fitness': 6}, {'genome': 'gene', 'fitness': 5}]
    [{'genome': 'meme', 'fitness': 6}, {'genome': 'gene', 'fitness': 5}]
    [{'genome': 'meme', 'fitness': 6}, {'genome': 'gene', 'fitness': 5}]
    """
    return individuals[0:pop_size]


def evolve(pop_size: int, generations: int = 100) -> Population:
    """
    Purpose:        A whole EC run, main driver
    Parameters:     The evolved population of solutions
    User Input:     No
    Prints:         Updates every time fitness switches.
    Returns:        Population
    Modifies:       Various data structures
    Calls:          Basic python only, all your functions
    Tests:          ./stdio_tests/* and ./arg_tests/
    Status:         Giving you this one.
    """
    # To debug doctest test in pudb
    # Highlight the line of code below below
    # Type 't' to jump 'to' it
    # Type 's' to 'step' deeper
    # Type 'n' to 'next' over
    # Type 'f' or 'r' to finish/return a function call and go back to caller
    population = initialize_pop(pop_size=pop_size)
    evaluate_group(individuals=population)
    rank_group(individuals=population)
    best_fitness = population[0]["fitness"]
    for i in range(generations):
        parents = parent_select(individuals=population, number=int(pop_size * 0.8))
        children = recombine_group(parents=parents, recombine_rate=0.8)
        mutate_rate = 0.9
        mutants = mutate_group(children=children, mutate_rate=mutate_rate)
        evaluate_group(individuals=mutants)
        everyone = population + mutants
        rank_group(individuals=everyone)
        population = survivor_select(individuals=everyone, pop_size=pop_size)
        if best_fitness != population[0]["fitness"]:
            best_fitness = population[0]["fitness"]
            print(
                "Iteration number",
                i + 1,
                "with best individual",
                population[0]["fitness"],
            )
            write_best(population[0])
    return population


if __name__ == "__main__":
    random.seed(24601)
    # OBJECTIVE = input("What string would you like to evolve?\n")
    # POP_SIZE = int(input("How many individuals would you like to evolve?\n"))
    population = evolve(100, 20)
