
# Robot Arm Simulation EC

To run this code, first clone it like usual, then download Godot 4.0 - Beta 3 from the link below, unzip it, and place it in the same directory. Python will call that executable file to evaluate the population.

https://downloads.tuxfamily.org/godotengine/4.0/beta3/
